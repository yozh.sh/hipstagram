import React from 'react';
import TextField from '@material-ui/core/TextField';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import { useState } from 'react'


function PasswordField(){
    const [passwordVisible, setPasswordVisible] = useState(false)
    return (
        <TextField size="small" type={passwordVisible ? 'text' : 'password' } label="Введiть пароль" variant="outlined"
            InputProps={{
                endAdornment:
                <IconButton
                  aria-label="toggle password visibility"
                  edge="end"
                  size="small"
                  onClick={() => {
                      setPasswordVisible(!passwordVisible)
                  }}
                >
                {passwordVisible ? <VisibilityOff /> : <Visibility />}
                </IconButton>
            }}
        />
    )
}

export default PasswordField;
