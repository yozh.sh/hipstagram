import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import loginSchema from './validateSchema'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import PasswordField from '@components/PasswordField/Password'


function LoginComponent({submitName}){
    const loginField = () => <TextField label="Ваш email" variant="outlined" size="small" />
    return (
        <div>
            <Formik
                initialValues={{ email: '', password: '' }}
                validationSchema={loginSchema}
                onSubmit={values => {
                    // same shape as initial values
                    console.log(values);
                  }}
            >
            {({ errors, touched }) => (
                <Form className="form">
                    <Field type='email' name='email' component={loginField}/>
                    <Field type='password' name='password' component={PasswordField}/>
                    <Button variant="contained" color="primary">{submitName || "Submit Button"}</Button>
                </Form>
       )}
                
            </Formik>
        </div>
    )
}

export default LoginComponent;
