import * as Yup from 'yup';


const loginSchema = Yup.object().shape({
    email: Yup.string().email('Ваш email неправильний').required('Обов`язкове поле'),
    password: Yup.string().min(4, 'Маеш закороткий пароль').required('Обов`язкове поле'),
})

export default loginSchema;
