import React from 'react';
import './index.css'


function TextLogo({text}){
    return (
    <div id='logo-text'>
        {text}
    </div>
    );
}

export default TextLogo;
