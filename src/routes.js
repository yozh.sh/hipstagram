const routeTable = {
    login: () => '/login',
    register: () => '/registration'
}

export default routeTable
