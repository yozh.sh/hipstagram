import React from 'react';
import TextLogo from '@components/TextLogo/TextLogo';
import Login from '@components/Login/Login';
import '@pages/Login/index.css'
import Button from '@material-ui/core/Button';
import { useHistory } from "react-router-dom";
import routeTable from '@src/routes'



export default function RegistrationPage(){
    const history = useHistory()

    function backToLogin(){
        history.push(routeTable.login())
    }

    return (
        <div className="content-container">
           <div></div>
           <div className="logo-box">
               <TextLogo text="Hipstagram"/>
           </div>
           <div></div>
           <div></div>
           <div className="logistration">
               <div></div>
               <div className="login-form">
                   <Login submitName="Створити аккаунт"/>
               </div>
               <div className="mobile-div"></div>
               <div className="mobile-div"></div>
               <div id="sign-up-btn">
                   <Button onClick={backToLogin} variant="outlined" color="primary">Вхiд</Button>
               </div>
               <div>
            </div>
           </div>
           <div></div>
           <div></div>
           <div></div>
        </div>
    )
}
