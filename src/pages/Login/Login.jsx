import React from 'react';
import TextLogo from '@components/TextLogo/TextLogo';
import Login from '@components/Login/Login';
import './index.css'
import Button from '@material-ui/core/Button';
import { useHistory } from "react-router-dom";
import routeTable from '@src/routes'


export default function LoginPage(){
    const history = useHistory()

    function toRegister(){
        history.push(routeTable.register())
    }
    return (
        <div className="content-container">
           <div></div>
           <div className="logo-box">
               <TextLogo text="Hipstagram"/>
           </div>
           <div></div>
           <div></div>
           <div className="logistration">
               <div></div>
               <div className="login-form">
                   <Login submitName="Вхiд"/>
               </div>
               <div className="mobile-div"></div>
               <div className="mobile-div"></div>
               <div id="sign-up-btn">
                   <Button onClick={toRegister} variant="outlined" color="primary">Реєстрацiя</Button>
                   <a className="forgot-password" href="#">Забули пароль?</a>
               </div>
               <div>
            </div>
           </div>
           <div></div>
           <div></div>
           <div></div>
        </div>
    )
}
