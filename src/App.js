import React from 'react';
import RegistrationPage from './pages/Registration/Registration'
import Login from '@pages/Login/Login'
import routeTable from '@src/routes'

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Switch>
            <Route path={routeTable.login()}>
              <Login/>
            </Route>
            <Route path={routeTable.register()}>
              <RegistrationPage/>
            </Route>
      </Switch>
    </div>
  );
}

export default App;
